package Controller;

import java.util.Comparator;

import Model.Monom;

public class SortByGrade implements Comparator<Monom> {
   
	public int compare(Monom a, Monom b)
	{
		return b.getGrade()-a.getGrade();
	}
}
