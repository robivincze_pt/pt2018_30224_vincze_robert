
package Controller;

import java.util.*;

import java.util.regex.*;

import javax.swing.JOptionPane;

import View.GUI;
import Model.AddOperation;
import Model.DeriveOperation;
import Model.DivideOperation;
import Model.IntegrateOperation;
import Model.Monom;
import Model.MultiplyOperation;
import Model.OperationType;
import Model.Operation;
import Model.Polynomial;
import Model.ShowPolynomial;
import Model.SubtractOperation;

public class MainController {

	private GUI polynomialGUI = new GUI();
	private String firstPolynomial;
	private String secondPolynomial;
	private Polynomial p1;
	private Polynomial p2;
	private Polynomial result = new Polynomial();
	ArrayList<Polynomial> res = new ArrayList<Polynomial>();

	public MainController() {
		this.polynomialGUI.addOkAction(e -> {
			// When we press the Ok Button, the two Polynomials
			// should be parsed

			// We erase the previous result. Normally it would be replaced by the new result
			// unless some error occurs.
			this.polynomialGUI.getResult().setText("");

			firstPolynomial = this.polynomialGUI.getFirstPolynomial();
			secondPolynomial = this.polynomialGUI.getSecondPolynomial();

			// Caracters can only be: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, +, -, ^, x
			if (!(this.checkString(firstPolynomial) && this.checkString(secondPolynomial))) {
				JOptionPane.showMessageDialog(this.polynomialGUI, "You entered invalid characters! \n");
			} else {
				p1 = this.parsePolynomial(firstPolynomial);
				p2 = this.parsePolynomial(secondPolynomial);

				String comboResult = polynomialGUI.comboText();
				Operation op;

				// Basically I divided the 6 operations into 2 groups, depending on the number
				// of results it
				// should return(1 in case of Add, Subtract, Multiply and 2 in case of Divide,
				// Derive, Integrate)

				// Where we need 2 results, we return them in the form of a 2 element Polynomial
				// Array
				if (comboResult.equals("Add")) {
					op = AddOperation.getInstance(OperationType.ADD, p1, p2);
					result = op.execute(p1, p2);
				} else if (comboResult.equals("Subtract")) {
					op = SubtractOperation.getInstance(OperationType.SUBTRACT, p1, p2);
					result = op.execute(p1, p2);
				} else if (comboResult.equals("Multiply")) {
					op = MultiplyOperation.getInstance(OperationType.MULTIPLY, p1, p2);
					result = op.execute(p1, p2);
				} else if (comboResult.equals("Divide")) {

					DivideOperation divOp = new DivideOperation();
					Collections.sort(p1.getTerms(), new SortByGrade());
					Collections.sort(p2.getTerms(), new SortByGrade());

					res = divOp.executeOperation(p1, p2);
					if (res.get(0).getTerms().get(0).getCoeff() == '&') {
						JOptionPane.showMessageDialog(this.polynomialGUI, "Division by 0 is not allowed!");
					}
				} else if (comboResult.equals("Get Derivative")) {
					DeriveOperation derOp = new DeriveOperation();
					res = derOp.executeOperation(p1, p2);
					if (res.get(0).getTerms().get(0).getCoeff() == '&') {
						JOptionPane.showMessageDialog(this.polynomialGUI, "Getting derivative of 0 is not possible!");
					}
				} else if (comboResult.equals("Get Integral")) {
					IntegrateOperation intOp = new IntegrateOperation();
					res = intOp.executeOperation(p1, p2);
				} else {
					ShowPolynomial p = new ShowPolynomial(p1);
					p.setVisible(true);
				}

				// We sort the results in decreasing order of the grade, to make the polynomials
				// look nicer if they
				// haven't been entered in the normal order by the user
				Collections.sort(result.getTerms(), new SortByGrade());

				// The showResults method is called in different ways depending on the
				// current operation
				if (comboResult.equals("Add") || comboResult.equals("Subtract") || comboResult.equals("Multiply")) {
					this.showResults(result, 0);
				} else if (comboResult.equals("Divide")) {
					this.showResults(res.get(0), 0);
					this.showResults(res.get(1), 1);
				} else if (comboResult.equals("Get Derivative")) {
					this.showResults(res.get(0), 0);
					this.showResults(res.get(1), 2);
				} else if(comboResult.equals("Get Integral")) {
					this.showResults(res.get(0), 0);
					this.showResults(res.get(1), 3);
				}
			}

		});

	}

	public Polynomial parsePolynomial(String p) {

		Polynomial poly = new Polynomial();

		// The regex I used for the polynomial
		String monomialPattern = "((-?\\d+(?x))?(-?[xX])(\\^(-?\\d+))?)|(-?\\d+)";

		Pattern pattern = Pattern.compile(monomialPattern);

		p = p.replaceAll("\\s+", "");

		Matcher matcher = pattern.matcher(p);

		while (matcher.find()) {

			Monom m = new Monom();

			// Each example before the if's shows an example to understand the types of
			// groups

			// CASE: 3x^2, -2x^3
			if (matcher.group(2) != null && matcher.group(6) == null && matcher.group(5) != null) {

				m.setCoeff(Integer.parseInt(matcher.group(2)));
				m.setGrade(Integer.parseInt(matcher.group(5)));

			}

			// CASE: x^2, -x^3 -> coefficient = 1 OR -1
			else if (matcher.group(1) != null && matcher.group(3) != null && matcher.group(4) != null
					&& matcher.group(5) != null) {

				if (matcher.group(3).charAt(0) == '-') {
					m.setCoeff(-1);

				} else {
					m.setCoeff(1);

				}

				m.setGrade(Integer.parseInt(matcher.group(5)));
			}

			// CASE: 3x, -2x -> exponent = 1
			else if (matcher.group(2) != null && matcher.group(4) == null && matcher.group(6) == null) {

				m.setCoeff(Integer.parseInt(matcher.group(2)));

				m.setGrade(1);

			}

			// CASE: x, -x -> coefficient = 1 OR -1, exponent = 1
			else if (matcher.group(2) == null && matcher.group(5) == null && matcher.group(6) == null) {

				if (matcher.group(3).charAt(0) == '-') {
					m.setCoeff(-1);

				} else {
					m.setCoeff(1);

				}

				m.setGrade(1);
			}

			// CASE: 3, -5 -> exponent = 0
			else if (matcher.group(6) != null) {

				m.setCoeff(Integer.parseInt(matcher.group(6)));

				m.setGrade(0);

			}

			poly.getTerms().add(m);
			m = null;

		}

		if (poly.getTerms().isEmpty()) {
			Monom m = new Monom();
			m.setCoeff(0);
			m.setGrade(0);
			poly.getTerms().add(m);
		}

		return poly;
	}

	// This method retransforms the Polynomial into a String so it can be shown
	// on the GUI
	public String parseToString(Polynomial p) {

		String s = "";
		int j = 0;

		if (p.getTerms().get(0).getCoeff() == '&') {
			return s;
		}

		for (Monom m : p.getTerms()) {

			// This little trick is done to get only 2 decimals in case that a division
			// operator doesn't return an integer result
			m.setCoeff(m.getCoeff() * 100);
			m.setCoeff(Math.round(m.getCoeff()));
			m.setCoeff(m.getCoeff() / 100);

			if (m.getCoeff() > 0 && !m.equals(p.getTerms().get(0))) {
				s = s + "+";
			}

			if (m.getCoeff() != 0 && (m.getCoeff() != 1 || j == p.getSize() - 1)) {

				if (m.getCoeff() != -1 || j == p.getSize() - 1) {
					s = s + m.getCoeff();
				} else {
					s = s + "-";
				}
			}
			if (m.getCoeff() != 0 && m.getGrade() != 0) {
				s = s + "x";
				if (m.getGrade() != 1 && m.getGrade() != -1) {
					s = s + "^" + m.getGrade();
				}
			}
			j++;
		}

		if (s.equals("")) {

			s = "0";
		}

		return s;
	}

	// This method shows the result/results on the GUI
	public void showResults(Polynomial p, int i) {

		String result = this.parseToString(p);

		if (i == 0) {
			this.polynomialGUI.getResult().setText(result);
			this.polynomialGUI.getSecondResultLabel().setVisible(false);
			this.polynomialGUI.getSecondResult().setVisible(false);
		} else {

			this.polynomialGUI.getSecondResult().setText(result);
			if (i == 1) {
				this.polynomialGUI.getSecondResultLabel().setText("Rest:");
			} else {
				this.polynomialGUI.getSecondResultLabel().setText("Result 2:");
			}

			this.polynomialGUI.getSecondResultLabel().setVisible(true);
			this.polynomialGUI.getSecondResult().setVisible(true);

			if (i == 3) {
				this.polynomialGUI.getResult().setText(this.polynomialGUI.getResult().getText() + "+c");
				this.polynomialGUI.getSecondResult().setText(result + "+c");

				if (this.polynomialGUI.getResult().getText().startsWith("0+")) {
					this.polynomialGUI.getResult().setText("c");
				}

				if (this.polynomialGUI.getSecondResult().getText().startsWith("0+")) {
					this.polynomialGUI.getSecondResult().setText("c");
				}
			}
		}

	}

	// This method checks if the entered strings only contain allowed characters
	public boolean checkString(String p) {
		if (!(p.matches("[0-9]+") || p.indexOf('x') >= 0 || p.indexOf('^') >= 0 || p.indexOf('+') >= 0
				|| p.indexOf('-') >= 0)) {
			return false;
		}

		return true;
	}

}
