package Model;

public class Monom {

	// Each monom must have a coefficient and a grade.
	private double coeff;
	private int grade;

	public Monom(double coeff, int grade) {
		this.coeff = coeff;
		this.grade = grade;
	}

	public Monom() {

	}

	public void setCoeff(double coeff) {
		this.coeff = coeff;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public double getCoeff() {
		return this.coeff;
	}

	public int getGrade() {
		return this.grade;
	}

}
