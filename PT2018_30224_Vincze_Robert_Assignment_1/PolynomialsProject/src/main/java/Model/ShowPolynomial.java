package Model;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ShowPolynomial extends JFrame {

	private Polynomial p;

	public ShowPolynomial(Polynomial p) {
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		setSize(480, 480);
		setResizable(false);
		this.p = p;
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		Line2D AxaX = new Line2D.Float(0, 240, 480, 240);
		Line2D AxaY = new Line2D.Float(240, 0, 240, 480);

		EvaluatePolynomial eP = new EvaluatePolynomial();

		g2.draw(AxaX);
		g2.draw(AxaY);

		for (int i = 0; i < 480; i += 10) {
			Line2D line = new Line2D.Float(i, 235, i, 245);
			Line2D line2 = new Line2D.Float(235, i, 245, i);
			g2.draw(line);
			g2.draw(line2);
		}

		for (int i = 240; i < 480; i += 10) {
			int y1 = eP.calculateValue(p, (i - 240) / 10);
			int y2 = eP.calculateValue(p, (i + 10 - 240) / 10);

			Line2D line = new Line2D.Float(i, (240 - y1 * 10), i + 10, (240 - y2 * 10));

			g2.draw(line);
		}

		for (int i = 240; i > 0; i -= 10) {
			int y1 = eP.calculateValue(p, (i - 240) / 10);
			int y2 = eP.calculateValue(p, (i - 10 - 240) / 10);

			Line2D line = new Line2D.Float(i, (240 - y1 * 10), i - 10, (240 - y2 * 10));
			g2.draw(line);
		}

	}

}
