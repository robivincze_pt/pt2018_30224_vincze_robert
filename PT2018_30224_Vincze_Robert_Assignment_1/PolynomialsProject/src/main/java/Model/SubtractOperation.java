package Model;

public class SubtractOperation extends Operation {

	public Polynomial execute(Polynomial p1, Polynomial p2) {
		Polynomial result = new Polynomial();

		// Here we subtract the elements with grades in both polynomials
		for (Monom object1 : p1.getTerms()) {

			for (Monom object2 : p2.getTerms()) {
				if (object1.getGrade() == object2.getGrade()) {
					Monom m = new Monom();
					m.setGrade(object1.getGrade());
					m.setCoeff(object1.getCoeff() - object2.getCoeff());

					result.getTerms().add(m);
					m = null;
				}
			}

		}

		// Here we add the elements with grades only in first polynomial

		int ok = 0;

		for (Monom object1 : p1.getTerms()) {

			for (Monom object2 : p2.getTerms()) {
				if (object1.getGrade() == object2.getGrade()) {
					ok = 1;
					break;
				}
			}

			if (ok == 0) {

				result.getTerms().add(object1);
			}

			ok = 0;
		}

		ok = 0;

		// Here we add the elements with grades only in the second polynomials, changing
		// the
		// sign of the coefficient, by subtracting it's double value.
		for (Monom object2 : p2.getTerms()) {

			for (Monom object1 : p1.getTerms()) {
				if (object1.getGrade() == object2.getGrade()) {
					ok = 1;
					break;
				}
			}

			if (ok == 0) {
				object2.setCoeff(object2.getCoeff() - 2 * object2.getCoeff());
				result.getTerms().add(object2);
			}

			ok = 0;
		}

		// We remove terms with 0 coefficient because they shouldn't exist.
		this.removeZeroTerms(result);

		return result;
	}
}
