package Model;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;

public class MultiplyOperation extends Operation{

	public Polynomial execute(Polynomial p1, Polynomial p2) {

		Polynomial result = new Polynomial();

		// Multiplying every term from the first with every term from the second
		for (Monom obj1 : p1.getTerms()) {
			for (Monom obj2 : p2.getTerms()) {
				Monom m = new Monom();

				m.setGrade(obj1.getGrade() + obj2.getGrade());
				m.setCoeff(obj1.getCoeff() * obj2.getCoeff());
				result.getTerms().add(m);
				m = null;
			}
		}

		// Add the terms with the same grade, and then delete the "duplicates" by
		// setting their coefficient to 0
		for (Monom obj1 : result.getTerms()) {
			for (Monom obj2 : result.getTerms()) {
				if (obj1.getGrade() == obj2.getGrade() && !obj1.equals(obj2)) {
					obj1.setCoeff(obj1.getCoeff() + obj2.getCoeff());
					obj2.setCoeff(0);

				}

			}
		}

		// We remove terms with 0 coefficient because they shouldn't exist.
		this.removeZeroTerms(result);

		return result;
	}

}
