package Model;

import java.util.ArrayList;
import java.util.Collections;

import Controller.SortByGrade;

public class DivideOperation extends SubtractOperation{

	private Polynomial result = new Polynomial();
	private Polynomial rest = new Polynomial();
	private Monom pRest; /// Partial result(the term of the result we currently added)
	private Monom aux = new Monom(); /// The term with the biggest grade from the second polynomial
	private Monom strong = new Monom(); /// The term with the biggest grade from the first polynomial
	private Monom pResult = new Monom(); /// Partial rest(the term of the rest we currently added)
	private ArrayList<Polynomial> resultAndRest = new ArrayList<Polynomial>(); /// The final result which will be returned

	/// We will store the result in an ArrayList of Polynomials made from 2
	/// elements.
	/// First one is the result, second one is the rest.
	public ArrayList<Polynomial> executeOperation(Polynomial p1, Polynomial p2) {

		// If the first polynomial is 0, then the result and rest are also 0
		if (p1.getTerms().get(0).getCoeff() == 0) {

			this.firstIsZero();

			/// If the second polynomial is 0, then division isn't possible and we will use
			/// the '&' special character to show an error message
			return resultAndRest;
		} else if (p2.getTerms().get(0).getCoeff() == 0) {

			this.secondIsZero();

			/// If the second polynomials grade is bigger than the first's, then the result
			/// is 0
			/// and the rest will be the first polynomial
			return resultAndRest;
		} else if (p1.getTerms().get(0).getGrade() < p2.getTerms().get(0).getGrade()) {

			this.firstIsHigher(p1);

			return resultAndRest;
		}

		strong = p1.getTerms().get(0);
		aux = p2.getTerms().get(0);

		while (aux.getGrade() <= strong.getGrade()) {
			// A function which prepares the partial rest for the next iteration if
			// necessary
			pResult.setCoeff(strong.getCoeff() / aux.getCoeff());
			pResult.setGrade(strong.getGrade() - aux.getGrade());

			result.getTerms().add(pResult);

			for (Monom temp : p2.getTerms()) {
				// Multiply the term you got before with the second polynomial
				pRest = new Monom();
				pRest.setCoeff(pResult.getCoeff() * temp.getCoeff());
				pRest.setGrade(pResult.getGrade() + temp.getGrade());

				rest.getTerms().add(pRest);

				pRest = null;
			}

			// Subtract and bring down the next term.
			rest = this.execute(p1, rest);

			p1 = rest;

			Collections.sort(rest.getTerms(), new SortByGrade());

			// Find the position of the term with the highest grade. This will be the new
			// strong.
			// If you can't find it, it means the division is done with no rest.
			int j = this.findPosition();

			if ((j + 1) >= rest.getTerms().size()) {
				break;
			}

			this.prepareForNext(j);

		}

		resultAndRest.add(result);
		resultAndRest.add(rest);

		return resultAndRest;
	}

	public void firstIsZero() {
		aux.setCoeff(0);
		aux.setGrade(0);
		result.getTerms().add(aux);
		strong.setCoeff(0);
		strong.setGrade(0);
		rest.getTerms().add(strong);
		resultAndRest.add(result);
		resultAndRest.add(rest);
	}

	public void secondIsZero() {
		aux.setCoeff('&');
		aux.setGrade(0);
		result.getTerms().add(aux);
		strong.setCoeff(0);
		strong.setGrade(0);
		rest.getTerms().add(strong);
		resultAndRest.add(result);
		resultAndRest.add(rest);
	}

	public void firstIsHigher(Polynomial p1) {
		aux.setCoeff(0);
		aux.setGrade(0);
		result.getTerms().add(aux);
		resultAndRest.add(result);
		resultAndRest.add(p1);
	}

	/// Finds the biggest term of the rest, which is needed for next iteration
	public int findPosition() {
		int j = 0;

		while (rest.getTerms().get(j).getCoeff() == 0) {

			if ((j + 1) >= rest.getTerms().size()) {
				break;
			}
			j++;
		}

		return j;
	}

	public void prepareForNext(int j) {
		/// Divide the terms with the highest powers
		strong.setCoeff(rest.getTerms().get(j).getCoeff());
		strong.setGrade(rest.getTerms().get(j).getGrade());
		if (aux.getGrade() <= strong.getGrade()) {
			pResult = null;
			pResult = new Monom();
			rest = null;
			rest = new Polynomial();
		}
	}
}
