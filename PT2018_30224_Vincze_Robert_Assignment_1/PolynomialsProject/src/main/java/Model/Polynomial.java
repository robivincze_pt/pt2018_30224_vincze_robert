package Model;

import java.util.*;
//A Polynomial is a List of Monoms.

public class Polynomial {

	private List<Monom> terms;

	public Polynomial() {
		this.terms = new ArrayList<Monom>();
	}

	public int getSize() {
		return this.terms.size();
	}

	public List<Monom> getTerms() {
		return this.terms;
	}
}
