package Model;

public class EvaluatePolynomial {

	public int calculateValue(Polynomial p, int point) {
		int value = 0;

		for (Monom m : p.getTerms()) {
			value += m.getCoeff() * Math.pow(point, m.getGrade());
		}

		return value;
	}

}
