package Model;

public enum OperationType {

	ADD, SUBTRACT, MULTIPLY, DIVIDE, GETDERIVATIVE, GETINTEGRAL
}
