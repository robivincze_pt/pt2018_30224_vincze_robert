package Model;

import Model.MultiplyOperation;

import java.util.ArrayList;

import Model.AddOperation;
import Model.SubtractOperation;

//This class will be used only by operations wich return a single result. 
public class Operation {

	public static Operation getInstance(OperationType oT, Polynomial p1, Polynomial p2) {
		if (oT == OperationType.ADD) {
			return new AddOperation();
		} else if (oT == OperationType.SUBTRACT) {
			return new SubtractOperation();
		} else {
			return new MultiplyOperation();
		}
	}

	// This method will be overwritten by certain operations
	public Polynomial execute(Polynomial p1, Polynomial p2) {
		return p1;
	}

	// This method builds a List of the terms which have 0 coefficient, and removes
	// them
	// from the Polynomials using this List
	public void removeZeroTerms(Polynomial p) {

		for (int i = 0; i < p.getSize(); i++) {
			if (p.getTerms().get(i).getCoeff() == 0) {
				p.getTerms().remove(i);
			}
		}
	}
}
