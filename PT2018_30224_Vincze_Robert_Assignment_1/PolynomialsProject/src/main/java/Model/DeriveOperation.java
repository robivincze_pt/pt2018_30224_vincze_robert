package Model;

import java.util.ArrayList;

public class DeriveOperation{

	public ArrayList<Polynomial> executeOperation(Polynomial p1, Polynomial p2) {
		ArrayList<Polynomial> results = new ArrayList<Polynomial>();

		// We need to show an Error Message if the user wants to get the derivative of 0
		if (p1.getTerms().get(0).getCoeff() == 0 || p2.getTerms().get(0).getCoeff() == 0) {
			p1.getTerms().get(0).setCoeff('&');
			results.add(p1);
			results.add(p2);

			return results;
		}

		for (Monom m : p1.getTerms()) {
			m.setCoeff(m.getCoeff() * m.getGrade());
			m.setGrade(m.getGrade() - 1);
		}

		for (Monom n : p2.getTerms()) {
			n.setCoeff(n.getCoeff() * n.getGrade());
			n.setGrade(n.getGrade() - 1);
		}

		results.add(p1);
		results.add(p2);

		return results;
	}
}