package Model;

import java.util.ArrayList;

public class IntegrateOperation{

	public ArrayList<Polynomial> executeOperation(Polynomial p1, Polynomial p2) {
		for (Monom m : p1.getTerms()) {
			m.setCoeff(m.getCoeff() / (m.getGrade() + 1));
			m.setGrade(m.getGrade() + 1);
		}

		for (Monom n : p2.getTerms()) {
			n.setCoeff(n.getCoeff() / (n.getGrade() + 1));
			n.setGrade(n.getGrade() + 1);
		}

		ArrayList<Polynomial> results = new ArrayList<Polynomial>();
		results.add(p1);
		results.add(p2);

		return results;
	}

}
