package View;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;

	private JTextField textField1;
	private JTextField textField2;
	private JTextField firstResult;
	private JTextField secondResult;
	private JComboBox comboBox;
	private JButton button;
	private JLabel polynomial1;
	private JLabel polynomial2;
	private JLabel resultLabel;
	private JLabel secondResultLabel;
	private JLabel selectOperation;
	private JLabel errorMessage;
	String operations[] = { "Add", "Subtract", "Multiply", "Divide", "Get Derivative", "Get Integral","Show Polynomial"};

	public GUI() {
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Polynomials Application");
		this.setBounds(100, 100, 590, 355);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);

		polynomial1 = new JLabel("Enter the first polynomial:");
		polynomial1.setForeground(Color.BLUE);
		polynomial1.setBounds(72, 69, 175, 14);

		polynomial2 = new JLabel("Enter the second polynomial:");
		polynomial2.setForeground(Color.BLUE);
		polynomial2.setBounds(72, 115, 175, 14);

		this.getContentPane().add(polynomial1);
		this.getContentPane().add(polynomial2);

		textField1 = new JTextField();
		textField1.setBounds(266, 66, 175, 20);
		textField1.setColumns(10);

		textField2 = new JTextField();
		textField2.setBounds(266, 112, 175, 20);
		textField2.setColumns(10);

		this.getContentPane().add(textField1);
		this.getContentPane().add(textField2);

		comboBox = new JComboBox<String>(operations);
		comboBox.setBounds(213, 166, 116, 20);

		this.getContentPane().add(comboBox);

		selectOperation = new JLabel("Select operation:");
		selectOperation.setBounds(69, 169, 116, 14);
		selectOperation.setForeground(Color.BLUE);

		this.getContentPane().add(selectOperation);

		button = new JButton("OK");
		button.setBounds(371, 165, 89, 23);
		this.getContentPane().add(button);

		resultLabel = new JLabel("Result:");
		resultLabel.setBounds(110, 218, 75, 14);
		this.getContentPane().add(resultLabel);

		secondResultLabel = new JLabel("Remainder:");
		secondResultLabel.setBounds(110, 240, 75, 14);
		this.getContentPane().add(secondResultLabel);

		firstResult = new JTextField();
		firstResult.setBounds(213, 215, 375, 20);
		firstResult.setColumns(10);
		this.getContentPane().add(firstResult);

		secondResult = new JTextField();
		secondResult.setBounds(213, 237, 375, 20);
		secondResult.setColumns(10);
		this.getContentPane().add(secondResult);

		secondResult.setVisible(false);
		secondResultLabel.setVisible(false);
	}

	public void addOkAction(ActionListener a) {
		this.button.addActionListener(a);
	}

	public String getFirstPolynomial() {
		return this.textField1.getText();
	}

	public String getSecondPolynomial() {
		return this.textField2.getText();
	}

	public String comboText() {
		return (String) this.comboBox.getSelectedItem();
	}

	public void setResult(String result) {
		this.firstResult.setText(result);
	}

	public void setSecondResult(String secondResult) {

		this.secondResult.setText(secondResult);
	}

	public JTextField getResult() {
		return firstResult;
	}

	public JTextField getSecondResult() {
		return secondResult;
	}

	public JLabel getSecondResultLabel() {
		return secondResultLabel;
	}

}
